#!/usr/bin/env python3

import argparse
import socket

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('type', help='Specify TCP or UDP')
    args = parser.parse_args()

    if args.type.upper() == "TCP":
        run_tcp_server()
    elif args.type.upper() == 'UDP':
        run_udp_server()
    else:
        print('Invalid server type. Must be TCP/tcp or UDP/udp.')

def run_tcp_server():
    server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server.bind(('127.0.0.1', 10000))
    server.listen()
    conn, addr = server.accept()

    with conn:
        print(f'Connected by {addr}')
        while True:
            data = conn.recv(20)
            data = data.decode()
            if data == 'close':
                conn.close()
                server.close()
                break
            print(data)

def run_udp_server():
    server = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    server.bind(('127.0.0.1', 10000))

    while True:
        data, addr = server.recvfrom(20)
        data = data.decode()
        if data == 'close':
            print(f'close message sent from {addr}')
            server.close()
            break
        print(f'{addr}: {data}')

main()
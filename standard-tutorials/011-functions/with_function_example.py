#!/usr/bin/env python3

def check_name(name):
    # check for numbers and special characters
    invalid = False
    if name[0] != name[0].upper():
        print('First letter in name is not capitalized')
        invalid = True
    for x in name:
        if not x.isalpha:
            print('ERROR: Name contains invalid characters')
            invalid = True

    return invalid

first = ''
middle = ''
last = ''

while True:
    print(f'Name: {first} {middle} {last}')
    print()
    print('1 ) Enter First Name')
    print('2 ) Enter Middle Name')
    print('3 ) Enter Last Name')
    choice = input('Select Menu option (q to quit): ')

    if choice == 'q':
        break
    elif choice == '1':
        fname = input('Enter First Name: ')
        if check_name(fname):
            print('Invalid First name, try again.')
            continue
        else:
            first = fname
    elif choice == '2':
        mname = input('Enter Middle Name: ')
        if check_name(mname):
            print('Invalid Middle name, try again.')
            continue
        else:
            middle = mname
    elif choice == '3':
        lname = input('Enter Last Name: ')
        if check_name(lname):
            print('Invalid Last name, try again.')
            continue
        else:
            last = lname
    else:
        print('Invalid menu choice, try again')
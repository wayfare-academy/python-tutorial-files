#!/usr/bin/env python3

def dec_func1(func):
    def wrapper_func(*args, **kwargs):
        print('FIRST DECORATOR CALLED')
        print(func)
        print()
        ret = func(*args, **kwargs)
        print('FIRST DECORATOR ENDED')
        return ret

    return wrapper_func

def dec_func2(func):
    def wrapper_func(*args, **kwargs):
        print('SECOND DECORATOR CALLED')
        print(func)
        print()
        ret = func(*args, **kwargs)
        print('SECOND DECORATOR ENDED')
        return ret

    return wrapper_func

@dec_func1
@dec_func2
def repeat(msg, count):
    return msg * count

res = repeat('This message may repeat\n', 2)
print(res)


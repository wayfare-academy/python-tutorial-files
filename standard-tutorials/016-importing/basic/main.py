from data.item import Armor, Weapon
from data.funcvars import (
    MAX,
    test,
    do_something
)

print('')
armor1 = Armor('Iron Helm', 'A helmet made of iron', 1.0, 'Common', 1, 'Head', 2.0)
weapon = Weapon('Iron sword', 'An iron sword', 1.0, 'Common', 1, 'RH', 4.0)

print()
print(MAX)
print(test)
do_something()
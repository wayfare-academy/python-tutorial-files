#! /usr/bin/env python3

# properties for lists

class Pilot:

    def __init__(self):
        self._data = [
            1, 2, 3, 4, 5
        ]

    def _get_data(self):
        return self._data

    data = property(_get_data, None, None,)


p = Pilot()

p.data[0] = -1
p.data.append(6)

print(p.data)
print(p._data)

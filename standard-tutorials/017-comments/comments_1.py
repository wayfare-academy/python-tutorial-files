#!/usr/bin/env python3

# imports
from stuff import Ship, print_ships

def main():

    # create ships
    ship1 = Ship('Hawk', 'Frigate', 200, 300) # this is a comment
    ship2 = Ship('Polaris', 'Cruiser', 400, 700)

    # assign pilots
    ship1.assign_pilot('Bob')
    ship2.assign_pilot('Krowso')

    # print out information using
    # function from stuff.py
    print_ships([ship1, ship2])

    #TODO: something else

main()
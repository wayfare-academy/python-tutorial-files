#!/usr/bin/env python3

'''
This example demonstrates how to pass functions into other functions as 
arguments.
'''

def func():
    print('----')
    print('This function did some things')
    print('----')

def func_wrapper(func_name):
    print('Do some stuff before function')
    func_name()
    print('Do some stuff after function')

func_wrapper(func)
import logging

logging.basicConfig(
    format='%(asctime)s | %(levelname)8s | %(message)s',
    level=logging.DEBUG,
    encoding='utf-8'
)

print()
logging.debug('This is a debug message')
logging.info('This is an info message')
logging.warning('This is a warning message')
logging.error('This is an error message')
logging.critical('This is a critical message')
print()
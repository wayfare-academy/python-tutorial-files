
def dec_func(func):
    def wrapper_func():
        print('Before the function call')
        print('---------------------------------')
        func()
        print('---------------------------------')
        print('After the function call')

    return wrapper_func

@dec_func
def print_msg():
    print('Messages from the actual function')


print_msg()
#!/usr/bin/env python3

# Properties with everything
class Pilot:

    def __init__(self, name, id):
        self._name = name
        self._id = id

    # getters
    def _get_name(self):
        return self._name
    
    def _get_id(self):
        return self._id
    
    # setters
    def _set_name(self, value):
        self._name = value
    
    def _set_id(self, value):
        self._id = value

    # deleters
    def _del_name(self):
        del self._name
    
    def _del_id(self):
        del self._id

    name = property(_get_name, _set_name, _del_name, 'The name of the pilot')
    p_id = property(_get_id, _set_id, _del_id, 'The ID of the pilot')


p1 = Pilot('Krowso', 123456)
p2 = Pilot('Anna', 234567)

print('\nInitial Values')
print('--------------')
print(f'{p1.name} - {p1.p_id}')
print(f'{p2.name} - {p2.p_id}')

print('\nChange Values')
print('-------------')
p1.name = 'Victor'
p2.p_id = '112233'
print(f'{p1.name} - {p1.p_id}')
print(f'{p2.name} - {p2.p_id}')

print('\nDelete values')
print('-------------')
del p1.name
del p2.p_id

if not hasattr(p1, '_name'):
    print('p1 _name attribute has been deleted')

if not hasattr(p2, '_id'):
    print('p1 _id attribute has been deleted')

print()
print(p1.name)
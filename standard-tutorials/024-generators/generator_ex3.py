#!/usr/bin/env python3

pilots = [
    {'pilot':'Pilot 1', 'shiptype': 'Frigate'},
    {'pilot':'Pilot 2', 'shiptype': 'Cargo'},
    {'pilot':'Pilot 3', 'shiptype': 'Cruiser'},
    {'pilot':'Pilot 4', 'shiptype': 'Capital'},
    {'pilot':'Pilot 5', 'shiptype': 'Frigate'},
    {'pilot':'Pilot 6', 'shiptype': 'Cargo'},
    {'pilot':'Pilot 7', 'shiptype': 'Blob'},
    {'pilot':'Pilot 8', 'shiptype': 'Capital'},
    {'pilot':'Pilot 9', 'shiptype': 'Frigate'},
    {'pilot':'Pilot 10', 'shiptype': 'Cargo'},
    {'pilot':'Pilot 11', 'shiptype': 'Cruiser'},
    {'pilot':'Pilot 12', 'shiptype': 'Capital'}
]

def get_cargo(p_list):
    for p in p_list:
        if p['shiptype'] not in ['Frigate', 'Cargo', 'Cruiser', 'Capital']:
            print(f'Invalid ship type detected: {p["shiptype"]}')
            return
        if p['shiptype'] == 'Cargo':
            yield p

for x in get_cargo(pilots):
    print(x)
#!/usr/bin/env python3

class Parent:

    def __init__(self, arg1):
        self.arg1 = arg1

    def func1(self):
        print(f'{type(self)} object called func1')


class Child(Parent):

    def __init__(self, arg1, arg2):
        self.arg2 = arg2
        super().__init__(arg1)

p = Parent('arg1')
p.func1()
c = Child('arg1', 'arg2')
c.func1()
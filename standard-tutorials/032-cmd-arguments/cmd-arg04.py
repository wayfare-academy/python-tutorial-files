#!/usr/bin/env python3

'''
Example 4 demonstrating subparsers.
'''

import argparse

parser = argparse.ArgumentParser(description=__doc__)
subparser = parser.add_subparsers(dest='command')

pilot_parser = subparser.add_parser('pilot', help='Add a pilot')
pilot_parser.add_argument('--name', required=True, type=str, help='Set pilot name')
pilot_parser.add_argument('--id', required=True, type=str, help='Set pilot ID')

ship_parser = subparser.add_parser('ship', help='Add a ship')
ship_parser.add_argument('--name', required=True, type=str, help='Set ship name')
ship_parser.add_argument('--type', required=True, type=str, help='Set ship type')

args = parser.parse_args()

if args.command == 'pilot':
    print(f'{args.name} : {args.id}')
elif args.command == 'ship':
    print(f'{args.name} : {args.type}')
#!/usr/bin/env python3

import socket
import json

def main():
    server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server.bind(('127.0.0.1', 10000))
    server.listen()
    conn, addr = server.accept()

    with conn:
        print(f'Connected by {addr}')
        data = conn.recv(100)
        data = data.decode()
        data = json.loads(data)

        for k in data.keys():
            if type(data[k]) is dict:
                print(f'{k} = ')
                for k2 in data[k]:
                    print(f'\t{k2} = {data[k][k2]}')
            else:
                print(f'{k} = {data[k]}')

        conn.close()
        server.close()

main()
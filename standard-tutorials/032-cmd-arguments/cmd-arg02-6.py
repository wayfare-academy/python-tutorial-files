#!/usr/bin/env python3

'''
Example 2 demonstrating how to add arguments.
'''

import argparse

parser = argparse.ArgumentParser(description=__doc__)

# restricting choices
parser.add_argument('--ship', choices=['frigate', 'cruiser', 'capital'], help='Set to one of 3 ships')

args = parser.parse_args()
print(args)

#!/usr/bin/env python3

if (x := 2) == 3:
    print(f'{x} == 3')
else:
    print(f'{x} =/= 3')
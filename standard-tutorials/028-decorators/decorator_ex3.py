#!/usr/bin/env python3

def dec_func(func):
    def wrapper_func(*args, **kwargs):
        ret = func(*args, **kwargs)
        return ret

    return wrapper_func

@dec_func
def repeat(msg, count):
    return msg * count

res = repeat('This message may repeat\n', 2)
print(res)

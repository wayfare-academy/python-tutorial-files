#! /usr/bin/env python3

import socket
import time

HOST = '127.0.0.1'
PORT = 10000

def main():
    buffer = ''
    recv = []
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as server:
        server.bind((HOST, PORT))
        server.listen()
        conn, addr = server.accept()
        print(f'Connected to {addr}')
        
        while True:
            size_header = conn.recv(4)
            if len(size_header) == 0:
                conn.close()
                break

            msg_len = int.from_bytes(size_header[0:4], byteorder='big')

            msg = b''
            msg_downloaded = 0
            while (msg_len - msg_downloaded) > 0:
                if (msg_len - msg_downloaded) >= 10:
                    data = conn.recv(10)
                    msg_downloaded += 10
                else:
                    data = conn.recv(msg_len-msg_downloaded)
                    msg_downloaded = msg_len
                    
                msg = msg + bytes(data)
                print(f'Message downloaded: {msg_downloaded}/{msg_len}\r', end='')
                time.sleep(.5)
            pad = ' ' * (45 - msg_len)
            print(f'{msg.decode()}{pad}')




main()
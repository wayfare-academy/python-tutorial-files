#!/usr/bin/env python3

def main():
    # conditional statements
    str1 = 'abcdef123'
    str2 = 'Krowso'
    str3 = '224466'

    print(f'str1 is alpha-numeric: {str1.isalnum()}')
    print(f'str2 is alphabetic: {str2.isalpha()}')
    print(f'str3 is numeric: {str3.isdigit()}')
    print(f'str2[0] is upper: {str2[0].isupper()}')
    print(f'str2[1] is upper: {str2[1].islower()}')

main()
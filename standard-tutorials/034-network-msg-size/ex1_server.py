#! /usr/bin/env python3

import socket

MSG_LEN = 45
HOST = '127.0.0.1'
PORT = 10000

def main():
    recv = []
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as server:
        server.bind((HOST, PORT))
        server.listen()
        conn, addr = server.accept()
        print(f'Connected to {addr}')
        print(f'Data received:')
        while True:
            data = conn.recv(MSG_LEN)
            if len(data) == 0:
                conn.close()
                break
            data = data.decode()
            print(data)
            recv.append(data.replace('#', ''))

    print('\nMessages with padding stripped:')
    for r in recv:
        print(r)
            
main()
#!/usr/bin/env python3

def main():
    # COMMON STRING FUNCTIONS
    message = 'This is a message to all pilots'
    # string split
    print(message)
    msg = message.split(' ')
    print(msg)

    # string join
    msg.extend(['in', 'the', 'system'])
    message = ' '.join(msg)
    print(message)

main()

# custom exception class
class BadShipTypeError(Exception):

    def __init__(self, shiptype, message='Invalid Ship Type'):
        self.shiptype = shiptype
        self.message = message
        super().__init__(f'{self.message}: {self.shiptype}')

# -----------------------------------------------------------
# code to trigger exception
def register_ship(shiptype, shipname):
    if shiptype in ['Frigate', 'Cruiser', 'Cargo']:
        print(f'{shiptype} registered: {shipname}')
    else:
        raise BadShipTypeError(shiptype)

def main():
    register_ship('Frigate', 'Hawk')
    register_ship('Blob', 'Morpho')

main()
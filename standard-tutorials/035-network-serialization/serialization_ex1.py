#!/usr/bin/env python3

import socket

def main():
    x = 1
    y = 2.5
    z = False
    name = 'Krowso'

    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as client:
        client.connect(('127.0.0.1', 10000))
        data = f'x={x};y={y};z={z};name={name}'
        client.send(data.encode())
        client.close()

main()

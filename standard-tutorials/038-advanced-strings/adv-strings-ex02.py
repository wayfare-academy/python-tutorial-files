#!/usr/bin/env python3

def main():

    # STRING CASTING AND CONVERSIONS
    print('CASTING WITH STRINGS')
    print('--------------------')
    num_str = '22'
    print(f'num_str type = {type(num_str)}')
    num = int(num_str)
    print(f'int(num_str) type = {type(num)}')
    print()

    bin_str = '1011'
    oct_str = '456'
    hex_str = 'F9'

    print(f'{bin_str} binary to decimal -> {int(bin_str, 2)}')
    print(f'{oct_str} octal to decimal -> {int(bin_str, 8)}')
    print(f'{hex_str} hexidecimal to decimal -> {int(bin_str, 16)}')
    print()

    char_code = 65
    char = 'X'

    print(f'ASCII character for {char_code}: {chr(char_code)}')
    print(f'Decimal value for {char}: {ord(char)}')

main()

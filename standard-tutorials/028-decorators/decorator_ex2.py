#!/usr/bin/env python3

def dec_func(func):
    def wrapper_func(*args, **kwargs):
        print('---------------------------------')
        print('Arguments passed in:')
        print(f'args: {args}')
        print(f'kwargs: {kwargs}')
        print('.................................')
        print('Function execution:')
        func(*args, **kwargs)
        print('---------------------------------')

    return wrapper_func

@dec_func
def print_msg():
    print('Messages from the actual function')

@dec_func
def repeat(msg, count):
    print(msg * count)

print_msg()
print()
print()
repeat('A ', 17)
repeat('B ', count = 10)
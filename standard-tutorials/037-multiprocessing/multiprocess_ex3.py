#!/usr/bin/env python3

import multiprocessing
import time
from colorama import Fore

dock1_1, dock1_2 = multiprocessing.Pipe()
dock2_1, dock2_2 = multiprocessing.Pipe()

class DockHandler(multiprocessing.Process):

    def __init__(self, name, color, pipe):
        super().__init__()
        self.name = name
        self.color = color
        self.pipe = pipe

    def run(self):

        while True:
            next = self.pipe.recv()
            if next == 'EOF':
                break
            print(f'{self.color}{self.name}: {next}{Fore.RESET}')
            time.sleep(.1)

def main():
    dh1 = DockHandler('DOCK 1', Fore.LIGHTBLUE_EX, dock1_2)
    dh2 = DockHandler('DOCK 2', Fore.YELLOW, dock2_2)

    dh1.start()
    dh2.start()

    for x in range(50):
        if x % 2 == 0:
            dock1_1.send(f'Ship {x} docked')
        else:
            dock2_1.send(f'Ship {x} docked')
        time.sleep(.1)

    for x in range(50):
        if x % 2 == 0:
            dock1_1.send(f'Ship {x} departed')
        else:
            dock2_1.send(f'Ship {x} departed')
        time.sleep(.1)

    dock1_1.send('EOF')
    dock2_1.send('EOF')

    dh1.join()
    dh2.join()

main()

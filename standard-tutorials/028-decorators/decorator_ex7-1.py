#!/usr/bin/env python3

def cls_decorator_func(cls):
    def wrapper(*args, **kwargs):
        print(f'Wrapping class: {cls}')
        obj = cls(*args, **kwargs)
        return obj
    return wrapper

@cls_decorator_func
class Pilot:

    def __init__(self, name):
        self.name = name

p = Pilot('Krowso')
print(p.name)
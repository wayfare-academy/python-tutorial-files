#! /usr/bin/env python3

# Property Decorator Example

class Pilot:

    def __init__(self, name, id):
        self._name = name
        self._id = id

    # NAME PROPERTY
    @property
    def name(self):
        return self._name
    
    @name.setter
    def name(self, value):
        self._name = value
    
    @name.deleter
    def name(self):
        del self._name
    
    # P_ID PROPERTY
    @property
    def p_id(self):
        return self._id
    
    @p_id.setter
    def p_id(self, value):
        self._id = value

    @p_id.deleter
    def p_id(self):
        del self._id

p1 = Pilot('Krowso', 123456)
p2 = Pilot('Anna', 234567)

print('\nInitial Values')
print('--------------')
print(f'{p1.name} - {p1.p_id}')
print(f'{p2.name} - {p2.p_id}')

print('\nChange Values')
print('-------------')
p1.name = 'Victor'
p2.p_id = '112233'
print(f'{p1.name} - {p1.p_id}')
print(f'{p2.name} - {p2.p_id}')

print('\nDelete values')
print('-------------')
del p1.name
del p2.p_id

if not hasattr(p1, '_name'):
    print('p1 _name attribute has been deleted')

if not hasattr(p2, '_id'):
    print('p2 _id attribute has been deleted')

print()
print(p1.name)
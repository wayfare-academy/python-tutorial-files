#!/usr/bin/env python3

pilot_list = [
    ('Tom', 123456, 'Frigate', 'Hawk'),
    ('Beth', 234567, 'Cargo', 'Crate'),
    ('Aron', 345678, 'Cruiser', 'Polaris')
]

def main():
    while show_menu():
        continue

def show_menu():
    print('1 ) Add new pilot')
    print('2 ) Remove existing pilot')
    print('3 ) Print pilot info')
    choice = input('Make a menu selection (q to quit): ')

    if choice == 'q':
        return False
    elif choice == '1':
        add_pilot()
    elif choice == '2':
        remove_pilot()
    elif choice == '3':
        print_pilot_info()
    else:
        print('Invalid Choice')

    return True

def add_pilot():
    name = input('Pilot Name: ')
    p_id = input('Pilot ID: ')
    ship_type = input('Ship Type: ')
    ship_name = input('Ship Name: ')
    pilot_list.append((name, int(p_id), ship_type, ship_name))

def remove_pilot():
    p_id = input('Enter pilot ID: ')
    p_id = int(p_id)
    for x in range(len(pilot_list)):
        if pilot_list[x][1] == p_id:
            del pilot_list[x]
            break

def print_pilot_info():
    p_id = input('Enter pilot ID: ')
    p_id = int(p_id)
    for x in range(len(pilot_list)):
        if pilot_list[x][1] == p_id:
            print(pilot_list[x])
            break

main()
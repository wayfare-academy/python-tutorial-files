def function1():
    raise FileExistsError()

def function2():
    raise ZeroDivisionError()

def function3():
    raise FileNotFoundError()

def main():
    try:
        function1()
        function2()
        function3()
    except Exception as e:
        print(type(e))

main()
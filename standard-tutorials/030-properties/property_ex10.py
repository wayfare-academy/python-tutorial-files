#! /usr/bin/env python3

# Various error handling

class Pilot:

    def __init__(self, fname, lname):
        self._first_name = fname
        self._last_name = lname

    @property
    def first_name(self):
        if hasattr(self, '_first_name'):
            return self._first_name
        else:
            # alternatively raise an error
            return None
    
    @property
    def last_name(self):
        # attribute should never be fully missing, at most
        # will be set to None
        return self._last_name
    
    @first_name.setter
    def first_name(self, value):
        # raise an error if not a str type
        if type(value) is not str:
            raise ValueError('first_name must be a string')
        self._first_name = value

    @last_name.setter
    def last_name(self, value):
        # raise an error if not a str type
        if type(value) is not str:
            raise ValueError('last_name must be a string')
        self._last_name = value

    @first_name.deleter
    def first_name(self):
        # if the attribute already doesn't exist, raise an error
        if hasattr(self, '_first_name'):
            del self._first_name
        else:
            raise AttributeError('Attribute _first_name does not exist')
    
    @last_name.deleter
    def last_name(self):
        # ensures that the attribute is never fully deleted
        self._last_name = None

# Test Code
p1 = Pilot('Krowso', 'Oswork')
print(f'{p1.first_name} {p1.last_name}')

try:
    p1.first_name = 12345
except Exception as e:
    print(e)

try:
    p1.last_name = True
except Exception as e:
    print(e)

del p1.first_name
del p1.last_name

print(p1.first_name)
print(p1.last_name)

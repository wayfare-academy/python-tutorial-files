import os
from pathlib import Path

# Read in data and replace part of the string with something else
fpath1 = os.path.join('.', 'out.txt')
data = []
fout = open(fpath1, 'r')
for line in fout:
    line = line.replace('first', '1st')
    line = line.replace('second', '2nd')
    line = line.replace('third', '3rd')
    line = line.replace('fourth', '4th')
    data.append(line)
fout.close

# write the modify data to another file
fpath2 = Path('out2.txt')
fin = open(fpath2, 'w')
for line in data:
    fin.write(f'{line}')
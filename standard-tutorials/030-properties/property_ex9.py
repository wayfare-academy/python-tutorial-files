#! /usr/bin/env python3

class Pilot:

    def __init__(self, fname, lname):
        self.first_name = fname
        self.last_name = lname

    @property
    def full_name(self):
        return f'{self.first_name} {self.last_name}'

p = Pilot('Victor', 'Rotciv')
print(p.full_name)
p.first_name = 'Krowso'
print(p.full_name)
p.last_name = 'Oswork'
print(p.full_name)
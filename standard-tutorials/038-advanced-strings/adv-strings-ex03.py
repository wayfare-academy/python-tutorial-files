#!/usr/bin/env python3

def main():
    # STRINGS AND OPERATORS
    str1 = 'aaa'
    str2 = 'aaa'
    str3 = 'AAA'
    str4 = 'abc'
    str5 = 'BBBaaaBBB'

    print(str1 + str3)
    print(str1 * 5)
    print()

    print(str1 == str2)
    print(str1 != str3)
    print()

    print(str1 > str3)
    print(str1 < str4)
    print()

    print(str1 in str5)
    print(str3 not in str5)
    print()
    
    print(str1 is str2)
    print(str1 is not str3)
    print(id(str1))
    print(id(str2))
    print(id(str3))
    print()

    print(type(str1) is str)
    print(id(type(str1)))
    print(id(str))

main()
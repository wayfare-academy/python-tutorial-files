#!/usr/bin/env python3

first = ''
middle = ''
last = ''

while True:
    print(f'Name: {first} {middle} {last}')
    print()
    print('1 ) Enter First Name')
    print('2 ) Enter Middle Name')
    print('3 ) Enter Last Name')
    choice = input('Select Menu option (q to quit): ')

    if choice == 'q':
        break
    elif choice == '1':
        fname = input('Enter First Name: ')

        # check for numbers and special characters
        invalid = False
        if fname[0] != fname[0].upper():
            print('First letter in name is not capitalized')
            invalid = True
        for x in fname:
            if not x.isalpha:
                print('ERROR: First name contains invalid characters')
                invalid = True
        if invalid:
            print('Invalid First name, try again.')
            continue
        else:
            first = fname
    elif choice == '2':
        mname = input('Enter Middle Name: ')

        # check for numbers and special characters
        invalid = False
        if mname[0] != mname[0].upper():
            print('First letter in name is not capitalized')
            invalid = True
        for x in mname:
            if not x.isalpha:
                print('ERROR: Middle name contains invalid characters')
                invalid = True
        if invalid:
            print('Invalid Middle name, try again.')
            continue
        else:
            middle = mname
    elif choice == '3':
        lname = input('Enter Last Name: ')

        # check for numbers and special characters
        invalid = False
        if lname[0] != lname[0].upper():
            print('First letter in name is not capitalized')
            invalid = True
        for x in lname:
            if not x.isalpha:
                print('ERROR: Last name contains invalid characters')
                invalid = True
        if invalid:
            print('Invalid Last name, try again.')
            continue
        else:
            last = lname
    else:
        print('Invalid menu choice, try again')
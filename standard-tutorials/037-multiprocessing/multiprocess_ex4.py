from colorama import Fore
import os
from multiprocessing import Pool
import time

def process_count(count):
    p_id = os.getpid()
    print(f'{Fore.RED}Process PID: {p_id}{Fore.RESET}')
    print(f'Process({p_id}) started')
    for x in range(count):
        print(f'Process({p_id}): counted {x} times')
        time.sleep(.1)
    print(f'Process({p_id}) completed')

def named_process_count(name, count):
    p_id = os.getpid()
    print(f'{Fore.RED}{name} PID: {p_id}{Fore.RESET}')
    print(f'{name} started')
    for x in range(count):
        print(f'{name}: counted {x} times')
        time.sleep(.1)
    print(f'{name} completed')

def main():
    print(f'{Fore.RED}Main PID: {os.getpid()}{Fore.RESET}')

    # starting processes for a function that only has one argument
    tpool = Pool(processes=2)
    tpool.map(process_count, [10, 20])

    # starting individual processes one at a time
    res = tpool.apply_async(process_count, (5,))
    res1 = tpool.apply_async(process_count, (6,))
    print(res.get(timeout=1))
    print(res1.get(timeout=1))

    # starting processes for a function that can have more than one argument
    tpool_res = []
    for args in [('Process 1', 5), ('Process 2', 5), ('Process 3', 5)]:
        tpool_res.append(tpool.apply_async(named_process_count, args))

    for res in tpool_res:
        print(res.get(timeout=1))

main()

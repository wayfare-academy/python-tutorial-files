#!/usr/bin/env python3

if (x := input('User input: ')) != '':
    print(x)
else:
    print('Input cannot be blank')
#!/usr/bin/env python3

def wrapper_func(func):
    def internal_func():
        print('Before the function call')
        print('---------------------------------')
        func()
        print('---------------------------------')
        print('After the function call')

    return internal_func

def print_msg():
    print('Messages from the actual function')

wrapper_func(print_msg)()

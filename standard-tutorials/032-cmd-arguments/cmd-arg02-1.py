#!/usr/bin/env python3

'''
Example 2 demonstrating how to add arguments.
'''

import argparse

parser = argparse.ArgumentParser(description=__doc__)

# adding arguments
#optional arguments as denoted by a minimum of 1 hyphen, unless otherwise specified
parser.add_argument('--word', type=str, help='Pass in a word')
parser.add_argument('-c', type=str, dest='character', help='Single character')

args = parser.parse_args()

print(args)
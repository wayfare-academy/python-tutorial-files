#!/usr/bin/env python3

def outer_dec_func(high_priority):
    def inner_dec_func(func):
        def wrapper_func(*args, **kwargs):
            ret = func(*args, **kwargs)
            if high_priority:
                mod = 'HIGH PRIORITY ALERT\n'
                mod += '-------------------\n'
                ret = mod + ret
            return ret[:-1]
        
        return wrapper_func

    return inner_dec_func

@outer_dec_func(True)
def repeat(msg, count):
    return msg * count

res = repeat('This message may repeat\n', 2)
print(res)


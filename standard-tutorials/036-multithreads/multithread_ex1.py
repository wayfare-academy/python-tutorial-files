#/usr/bin/env python
from colorama import Fore
import os
import threading
import time

def threaded_count(name, count):
    print(f'{Fore.RED}{name} PID: {os.getpid()}{Fore.RESET}')
    print(f'{name} thread started')
    for x in range(count):
        print(f'{name} thread: counted {x} times')
        time.sleep(.1)
    print(f'{name} thread completed')

def main():
    print(f'{Fore.RED}Main PID: {os.getpid()}{Fore.RESET}')
    t1 = threading.Thread(target=threaded_count, args=('First Counter', 10))
    t2 = threading.Thread(target=threaded_count, args=('Second Counter', 10))

    t1.start()
    t2.start()

    for x in range(10):
        print(f'Main thread: counted {x} times')
        time.sleep(.1)
    
    t1.join()
    t2.join()

main()
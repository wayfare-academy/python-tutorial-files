#!/usr/bin/env python3

# File IO without with
fout1 = open('out1.txt', 'w')
fout1.write('Line 1\n')
fout1.write('Line 2\n')
fout1.write('Line 3\n')

if not fout1.closed:
    fout1.close()
    print('fout1 is closed')

# File IO using with
with open('out2.txt', 'w') as fout2:
    fout2.write('Line 1\n')
    fout2.write('Line 2\n')
    fout2.write('Line 3\n')

if fout2.closed:
    print('fout2 is closed')

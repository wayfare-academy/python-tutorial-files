
class Config:

    def __init__(self, log_path, verbose, other_settings):
        self.log_path = log_path
        self.verbose = verbose
        self.other_settings = other_settings

    def __del__(self):
        print('del was used')

a = Config('logs/', True, {'more_settings':'yes'})
b = a
print('Deleting a')
del a
print('End of program')

#!/usr/bin/env python3

'''
This example demonstrates how function names can be stored into variables and
executed from said variables.
'''

def func1():
    print('hello')

print()
print('Executing func1 function:')
print('-------------------------')

# Example Code
func1()

print()
print('Assigning function itself to variable:')
print('--------------------------------------')

# Example Code
a_func_var = func1
print(a_func_var)
print(type(a_func_var))
a_func_var()
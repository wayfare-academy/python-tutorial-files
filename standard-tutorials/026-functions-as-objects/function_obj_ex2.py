#/usr/bin/env python3

'''
This example demonstrates how useful storing function names in variables can
be with python generators.
'''

def alpha_func():
    for x in ['a', 'b', 'c', 'd', 'e', 'f', 'h']:
        yield x

def numeric_func():
    for x in range(0, 11):
        yield x

gen = None
setting = 'num'

if setting == 'alpha':
    gen = alpha_func
elif setting == 'num':
    gen = numeric_func

for i in gen():
    print(i)

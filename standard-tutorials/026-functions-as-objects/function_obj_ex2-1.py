#/usr/bin/env python3

'''
This example demonstrates that arguments can still be passed into functions
even after being stored in a different variable name.
'''

def numeric_func(num):
    for x in range(0, num+1):
        yield x

gen = numeric_func

for i in gen(10):
    print(i)
#!/usr/bin/env python3

class Pilot:

    def __init__(self, name='Krowso', id=123456):
        self.name = name
        self.id = id
        Pilot.pilot_count +=1

    def greet(self):
        print(f'Hello, my name is {self.name}')

    def present_id(self):
        print(f'My pilot ID is {self.id}')

    def set_ship(self, ship_name):
        self.ship = ship_name

x = Pilot()

if hasattr(x, 'ship'):
    print('Ship found')
    print(getattr(x, 'ship'))
else:
    print('Ship not found, assigning ship')
    setattr(x, 'ship', 'Frigate')
    print(x.ship)

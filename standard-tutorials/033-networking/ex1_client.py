#!/usr/bin/env python3

import argparse
import socket

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('type', help='Specify TCP or UDP')
    args = parser.parse_args()

    if args.type.upper() == "TCP":
        run_tcp_client()
    elif args.type.upper() == 'UDP':
        run_udp_client()
    else:
        print('Invalid client type. Must be TCP/tcp or UDP/udp.')

def run_tcp_client():
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as client:
        client.connect(('127.0.0.1', 10000))
        while True:
            u_in = input('(close to disconnect)> ')
            client.send(u_in.encode())
            if u_in == 'close':
                client.close()
                break

def run_udp_client():
    with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as client:
        while True:
            u_in = input('(close to disconnect)> ')
            client.sendto(u_in.encode(), ('127.0.0.1', 10000))
            if u_in == 'close':
                client.close()
                break

main()
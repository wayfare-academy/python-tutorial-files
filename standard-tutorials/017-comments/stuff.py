'''
The docstring for stuff.py module.
'''
print(__doc__)
class Ship:
    '''
    This multiline comment is the docstring for the ship class.
    '''

    def __init__(self, name, type, shield, hull):
        '''
        This multiline comment is for docstring of __init__ of ship.
        '''
        self.name = name
        self.type = type
        self.shield = shield
        self.hull = hull

    def assign_pilot(self, pilot_name):
        '''
        This object method assigns a pilot name to the ship object.
        '''
        self.pilot = pilot_name

def print_ships(slist):
    '''
    This function prints out information about ships contained in a list.
    '''
    print('List of ships')
    print('-------------')
    for s in slist:
        print(f'Ship Name: {s.name}')
        print(f'Ship Pilot: {s.pilot}')
        print(f'Ship Type: {s.type}')
        print(f'Ship Shield: {s.shield}')
        print(f'Ship Hull: {s.hull}')
        print()
from pathlib import Path

fpath = Path(__file__).parent / 'pilot_db.csv'

#read from csv file and parse data
p_data = []
with open(fpath, 'r') as fin:
    for line in fin:
        line = line.strip('\n')
        p_data.append(tuple(line.split(',')))

print(p_data)

# convert data to YAML
ypath = fpath.parent / 'pilot_info.yml'
with open(ypath, 'w') as fout:
    fout.write('pilots:\n')
    for p in p_data:
        fout.write(f'  - name: \"{p[0]}\"\n')
        fout.write(f'    id: \"{p[1]}\"\n')
        fout.write(f'    ship_type: \"{p[2]}\"\n')
        fout.write(f'    ship_name: \"{p[3]}\"\n')
import logging

red = '\x1b[38;5;196m'
orange = '\x1b[38;5;202m'
yellow = '\x1b[38;5;226m'
blue = '\x1b[38;5;21m'
nc = '\x1b[0m'

class ColorFormatter(logging.Formatter):
    def __init__(self, fmt):
        super().__init__()
        self.fmt = fmt
        self.FORMATS = {
            logging.DEBUG: self.fmt,
            logging.INFO: blue + self.fmt + nc,
            logging.WARNING: yellow + self.fmt +nc,
            logging.ERROR: orange + self.fmt + nc,
            logging.CRITICAL: red + self.fmt + nc
        }

    def format(self, record):
        log_fmt = self.FORMATS.get(record.levelno)
        formatter = logging.Formatter(log_fmt)
        return formatter.format(record)


c_log = logging.getLogger('custom_logger')
c_log.setLevel(logging.DEBUG)

sh = logging.StreamHandler()

c_form = ColorFormatter('%(asctime)s | %(levelname)8s | %(message)s')

sh.setFormatter(c_form)

c_log.addHandler(sh)

print()
c_log.debug('This is a debug message')
c_log.info('This is an info message')
c_log.warning('This is a warning message')
c_log.error('This is an error message')
c_log.critical('This is a critical message')
print()

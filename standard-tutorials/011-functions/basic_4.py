 #!/usr/bin/env python3

def say_hello(id=12345, ship='hawk', name='User'):
    print(f'Name: {name}')
    print(f'ID: {id}')
    print(f'Ship: {ship}')

say_hello()
say_hello(ship='Cargo', id=23456, name='Krowso')

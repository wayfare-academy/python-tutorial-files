#!/usr/bin/env python3

def main():
    #string strip, lstrip, rstrip, replace 
    msg2 = '|T.h.i.s-i.s-a-g.a.r.b.l.e.d-m.e.s.s.a.g.e|'
    print(msg2.lstrip('|'))
    print(msg2.rstrip('|'))
    print(msg2.strip('|'))
    print(msg2.strip('|T'))
    print(msg2.replace('.', ''))

    new_msg2 = msg2.lstrip('|').rstrip('|').replace('.', '').replace('-', ' ').replace('garbled', 'ungarbled')
    print(new_msg2)

main()
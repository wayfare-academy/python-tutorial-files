
def outer_func():
    def inner_func():
        print('Called from the inner function')

    inner_func()

outer_func()

#!/usr/bin/env python3
class CustomWriter:

    def __init__(self, fname):
        self.fname = fname

    def __enter__(self):
        print('CustomWriter object as entered with statement')
        self.file = open(self.fname, 'w')
        return self.file
    
    def __exit__(self, *args):
        print(f'CustomWriter object has left with statement')
        self.file.close()

with CustomWriter('out1.txt') as cw:
    print('Writing lines to file')
    cw.write('CW Line 1\n')
    cw.write('CW Line 2\n')
    cw.write('CW Line 3\n')

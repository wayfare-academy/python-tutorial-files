import threading
import time
from colorama import Fore

dock1_queue = []
dock2_queue = []

class DockHandler(threading.Thread):

    def __init__(self, name, color, queue):
        super().__init__()
        self.name = name
        self.color = color
        self.queue = queue
        self.running = True

    def run(self):

        while self.running == True:
            if len(self.queue) == 0:
                time.sleep(.1)
                continue
            next = self.queue.pop(0)
            print(f'{self.color}{self.name}: {next}{Fore.RESET}')
            time.sleep(.1)

def main():
    dh1 = DockHandler('DOCK 1', Fore.LIGHTBLUE_EX, dock1_queue)
    dh2 = DockHandler('DOCK 2', Fore.YELLOW, dock2_queue)

    dh1.start()
    dh2.start()

    for x in range(50):
        if x % 2 == 0:
            dock1_queue.append(f'Ship {x} docked')
        else:
            dock2_queue.append(f'Ship {x} docked')
        time.sleep(.1)

    for x in range(50):
        if x % 2 == 0:
            dock1_queue.append(f'Ship {x} departed')
        else:
            dock2_queue.append(f'Ship {x} departed')
        time.sleep(.1)

    while len(dock1_queue) > 0 or len(dock2_queue) > 0:
        continue

    dh1.running = False
    dh2.running = False

    dh1.join()
    dh2.join()

main()
#!/usr/bin/env python3

'''
Example 2 demonstrating how to add arguments.
'''

import argparse

parser = argparse.ArgumentParser(description=__doc__)

# metavariables
parser.add_argument('--number', metavar='N', type=int, help='Pass in a number')
# this one does not require the argument name itself, positionals must be placed at the end
# of command
parser.add_argument('float', metavar='F', type=float, help='Required Positional argument')

args = parser.parse_args()
print(args)

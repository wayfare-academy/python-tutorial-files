#!/usr/bin/env python3

'''
Example 2 demonstrating how to add arguments.
'''

import argparse

parser = argparse.ArgumentParser(description=__doc__)

# set as required
parser.add_argument('--req', action='store_true', required=True, help='A required argument')

args = parser.parse_args()
print(args)
#!/usr/bin/env python3

from colorama import Fore
import multiprocessing
import os
import time

def process_count(name, count):
    print(f'{Fore.RED}{name} PID: {os.getpid()}{Fore.RESET}')
    print(f'{name} Process started')
    for x in range(count):
        print(f'{name} Process: counted {x} times')
        time.sleep(.1)
    print(f'{name} Process completed')

def main():
    print(f'{Fore.RED}Main PID: {os.getpid()}{Fore.RESET}')
    t1 = multiprocessing.Process(target=process_count, args=('First Process', 10))
    t2 = multiprocessing.Process(target=process_count, args=('Second Process', 10))

    t1.start()
    t2.start()

    for x in range(10):
        print(f'Main Process: counted {x} times')
        time.sleep(.1)
    
    t1.join()
    t2.join()

main()
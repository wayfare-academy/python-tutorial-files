#!/usr/bin/env python3

# scope: nonlocal keyword

def submit_name(fullname):
    def correct_casing(name):
        tmp = name[0].upper()
        for i in range(1, len(name)):
            tmp += name[i].lower()

        return tmp

    def check_casing():
        nonlocal first
        nonlocal last
        first = correct_casing(first)
        last = correct_casing(last)
    
    first, last = fullname.split(' ')
    check_casing()

    return f'{first} {last}'

print(submit_name('beTh anDErsOn'))

#!/usr/bin/env python3

# scope: variable access between inner and outer functions part 2

def outer():
    def inner():
        print(num)

    num = 2
    inner()


outer()

#!/usr/bin/env python3

# scope: variable access between inner and outer functions part 1

def outer(x):
    def inner():
        x = 4
        print()
        print(f'x = {x} during inner function') 
        print()
    print(f'x = {x} before inner function')
    inner()
    print(f'x = {x} after inner function')

outer(2)

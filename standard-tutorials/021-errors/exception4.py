import traceback
try:
    x = 2/0
    print(x)
except ZeroDivisionError as e:
    traceback.print_tb(e.__traceback__)
    print('-----')
    print(e)
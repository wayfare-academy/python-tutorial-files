#!/usr/bin/env python3

class Pilot:

    MAX_PILOTS = 3
    pilot_count = 0

    def __init__(self, name='Krowso', id=123456):
        self.name = name
        self.id = id
        Pilot.pilot_count +=1

    def greet(self):
        print(f'Hello, my name is {self.name}')

    def present_id(self):
        print(f'My pilot ID is {self.id}')

    def set_ship(self, ship_name):
        self.ship = ship_name

pdata = [
    {'name':'Krowso', 'id':123456},
    {'name':'Beth', 'id':234567},
    {'name':'Aron', 'id':345678},
    {'name':'???', 'id':111111},
]

plist = []
for x in pdata:
    if Pilot.pilot_count < Pilot.MAX_PILOTS:
        plist.append(Pilot(x['name'], x['id']))
    else:
        print('Cannot create anymore pilots')

for p in plist:
    print(f'{p.name}: {p.id}')

#/usr/bin/env python
from colorama import Fore
import os
from multiprocessing import Pool
import time

def threaded_count(count):
    p_id = os.getpid()
    print(f'{Fore.RED}Thread PID: {p_id}{Fore.RESET}')
    print(f'Thread({p_id}) started')
    for x in range(count):
        print(f'Thread({p_id}): counted {x} times')
        time.sleep(.1)
    print(f'Thread({p_id}) completed')

def named_threaded_count(name, count):
    p_id = os.getpid()
    print(f'{Fore.RED}{name} PID: {p_id}{Fore.RESET}')
    print(f'{name} started')
    for x in range(count):
        print(f'{name}: counted {x} times')
        time.sleep(.1)
    print(f'{name} completed')

def main():
    print(f'{Fore.RED}Main PID: {os.getpid()}{Fore.RESET}')

    # starting processes for a function that only has one argument
    tpool = Pool(processes=2)
    tpool.map(threaded_count, [10, 20])

    # starting individual processes one at a time
    res = tpool.apply_async(threaded_count, (5,))
    res1 = tpool.apply_async(threaded_count, (6,))
    print(res.get(timeout=1))
    print(res1.get(timeout=1))

    # starting processes for a function that can have more than one argument
    tpool_res = []
    for args in [('Thread 1', 5), ('Thread 2', 5), ('Thread 3', 5)]:
        tpool_res.append(tpool.apply_async(named_threaded_count, args))

    for res in tpool_res:
        print(res.get(timeout=1))

main()


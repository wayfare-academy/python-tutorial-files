#!/usr/bin/env python3

'''
This is the main module. It executes the code.
We would also place information about developers/owners,
copyright, and licensing stuff here when necessary.
'''
# imports
from stuff import Ship, print_ships

def main():
    '''
    This is a multiline comment for the main function.
    '''
    print(__doc__)
    print(main.__doc__)
    print(Ship.__doc__)
    print(Ship.__init__.__doc__)
    print(Ship.assign_pilot.__doc__)
    print(print_ships.__doc__)

main()

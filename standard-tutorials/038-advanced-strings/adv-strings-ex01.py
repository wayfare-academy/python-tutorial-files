#!/usr/bin/env python3

def main():
    #STRING BASICS
    name = 'Krowso'
    print(f'Name: {name}')
    
    # STRING INDEXING AND SLICING
    print(f'Length of name: {len(name)}')
    print(f'name[0] = {name[0]}')
    print(f'name[0:4] = {name[0:4]}')
    print(f'name[:4] = {name[:4]}')
    print(f'name[4:] = {name[4:]}')
    print(f'name[:-2] = {name[:-2]}')
    print(f'name[-2:] = {name[-2:]}')
    print(f'name[::2] = {name[::2]}')
    print(f'name[1::2] = {name[1::2]}')
    print(f'name[2::2] = {name[2::2]}')
    print(f'name[::-1] = {name[::-1]}')
    print(f'name[2::-1] = {name[2::-1]}')

    # results of indexing and slicing can be stored in a variable
    name_mod = name[::-1]
    print(name_mod)

main()
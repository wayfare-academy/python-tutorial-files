#!/usr/bin/env python3

'''
Example 3 demonstrating argument groups.
'''

import argparse

parser = argparse.ArgumentParser(description=__doc__)

pgroup = parser.add_mutually_exclusive_group()
pgroup.add_argument('--pilot-name', help='Specify pilot name')
pgroup.add_argument('--ship-name', help='Specify ship name')

args = parser.parse_args()

print()
print(args)
print()

#!/usr/bin/env python3

'''
Example 2 demonstrating how to add arguments.
'''

import argparse
from datetime import date

parser = argparse.ArgumentParser(description=__doc__)

# setting arguments with default values and defining actions
parser.add_argument('--debug', default=False, action='store_true', help='Turn debug on')
parser.add_argument('--no-verbose', dest='verbose', default=True, action='store_false', help='Turn verbose off')
parser.add_argument('--date', action='store_const', const=date.today(), help='Sets date when used')
parser.add_argument('--pilot', action='append', type=str, help='Appends multiple use to list')
parser.add_argument('-v', action='store_true', help='Groupable flags')
parser.add_argument('-d', action='store_true', help='Groupable flags')

args = parser.parse_args()
print(args)


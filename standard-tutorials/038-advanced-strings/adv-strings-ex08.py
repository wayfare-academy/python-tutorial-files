#!/usr/bin/env python3

def main():
    #SEARCHING STRINGS

    msg = 'Frigate Ship; Cruiser Ship; Capital Ship'
    print(msg.count('Ship'))
    print(msg.find('Cruiser'))
    print(msg.index('Capital'))

    msg = 'Frigate Ship'
    print(msg.startswith('Fri'))
    print(msg.endswith('hip'))

main()
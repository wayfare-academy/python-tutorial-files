#!/usr/bin/env python3

import socket

def main():
    server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server.bind(('127.0.0.1', 10000))
    server.listen()
    conn, addr = server.accept()

    with conn:
        print(f'Connected by {addr}')
        data = conn.recv(100)
        data = data.decode()
        data = data.split(';')
        for d in data:
            print(f'{d}')
        conn.close()
        server.close()

main()

import entry_point.second_data.second

class Item:
    
    def __str__(self):
        ret = f'Name {self.name}\nDescription {self.desc}\nValue: {self.value}'
        ret += f'\nRarity: {self.rarity}\nCount {self.count}'
        return ret

    def __init__(self, name, desc, value, rarity, count):
        self.name = name
        self.desc = desc
        self.value = value
        self.rarity = rarity
        self.count = count
        print(f'Created Item object {self.name}')

class Equipment(Item):
    
    def __str__(self):
        return f'{super().__str__()}\nSlot: {self.slot}'

    def __init__(self, name, desc, value, rarity, count, slot):
        Item.__init__(self, name, desc, value, rarity, count)
        self.slot = slot
        print(f'Created Equipment object {self.name}')

    def equip(self):
        print(f'Equipped {self.name} to {self.slot}')

class Armor(Equipment):

    def __str__(self):
        return f'{super().__str__()}\nDefense: {self.defense}'

    def __init__(self, name, desc, value, rarity, count, slot, defense):
        super().__init__(name, desc, value, rarity, count, slot)
        self.defense = defense
        print(f'Created Armor object {self.name}')

    def get_defense(self):
        return self.defense

class Weapon(Equipment):

    def __init__(self, name, desc, value, rarity, count, slot, damage):
        super(Weapon, self).__init__(name, desc, value, rarity, count, slot)
        self.damage = damage
        print(f'Created Weapon object {self.name}')

    def attack(self):
        return self.damage

    def equip(self):
        print('Checking weapon skill')
        super().equip()

print('item.py imported')
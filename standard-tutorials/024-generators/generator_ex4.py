#!/usr/bin/env python3

num_list = [1, 2, 3, 4, 5, 15, 20, 32, 33, 45, 50]
by_2 = []
by_3 = []
neither = []


def get_div(n_list):
    for n in n_list:
        if n % 2 == 0:
            yield 2, n
        elif n % 3 == 0:
            yield 3, n
        else:
            yield 0, n

for x in get_div(num_list):
    if x[0] == 2:
        by_2.append(x[1])
    elif x[0] == 3:
        by_3.append(x[1])
    else:
        neither.append(x[1])

print(f'Original Number List: {num_list}')
print('---------')
print(f'Divisble by 2: {by_2}')
print(f'Divisble by 3: {by_3}')
print(f'Divisble by neither: {neither}')
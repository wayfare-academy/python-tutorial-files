
class Config:
    instance = None

    def __new__(cls, *args, **kwargs):
        if Config.instance is None:
            cls.instance = super(Config, cls).__new__(cls)
        else:
            print('Singleton instance already exists')

        return cls.instance
    
    def __init__(self, log_path, verbose, other_settings):
        self.log_path = log_path
        self.verbose = verbose
        self.other_settings = other_settings

a = Config('logs/', True, {'more_settings':'yes'})
print('a object')
print(a.log_path)
print(a.verbose)
print(a.other_settings)
print('-------')
b = Config('another_path/', False, {'more_settings':'no'})
print('b object')
print(b.log_path)
print(b.verbose)
print(b.other_settings)
print('-------')
print(f'a id: {id(a)}')
print(f'b id: {id(b)}')

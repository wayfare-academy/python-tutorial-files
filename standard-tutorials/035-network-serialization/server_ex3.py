#!/usr/bin/env python3

import socket
import pickle

class Pilot:

    def __init__(self, name, id):
        self.name = name
        self.id = id

    def add_ship(self, ship):
        self.ship = ship

class Ship:
    
    def __init__(self, name, typ):
        self.name = name
        self.type = typ

def main():
    server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server.bind(('127.0.0.1', 10000))
    server.listen()
    conn, addr = server.accept()

    with conn:
        print(f'Connected by {addr}')
        data = conn.recv(200)
        print(data)
        obj = pickle.loads(data)
        print()
        print(type(obj))
        print(f'Pilot Name: {obj.name}')
        print(f'Pilot ID: {obj.id}')
        print(f'Ship Name: {obj.ship.name}')
        print(f'Ship Type: {obj.ship.type}')
        conn.close()
        server.close()

main()

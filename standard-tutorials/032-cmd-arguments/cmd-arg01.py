#!/usr/bin/env python3

'''
A docstring for this example. We can use this as the
description value for the ArgumentParser object. This
will get printed whenever the -h flag or an incorrect
argument is passed in.
'''

import argparse
import sys

print(sys.argv)
print()

parser = argparse.ArgumentParser(description=__doc__)

parser.add_argument('--an-arg', type=int, help='An argument')

# two ways to parse arguments: parse_args with no values passed in will
# parse all arguments given to program. parse_args with the parameter args
# being assigned
args = parser.parse_args()
#args = parser.parse_args(args=sys.argv[1:])

print(args.an_arg)
print('Args parsed successfully')
import logging

c_log = logging.getLogger('custom_logger')
c_log.setLevel(logging.DEBUG)

sh = logging.StreamHandler()
fh = logging.FileHandler('./log3.out')

c_form = logging.Formatter('%(asctime)s | %(levelname)8s | %(message)s')
f_form = logging.Formatter('%(asctime)s | %(name)s | %(levelname)8s | %(message)s')

sh.setFormatter(c_form)
fh.setFormatter(f_form)

c_log.addHandler(sh)
c_log.addHandler(fh)

print()
c_log.debug('This is a debug message')
c_log.info('This is an info message')
c_log.warning('This is a warning message')
c_log.error('This is an error message')
c_log.critical('This is a critical message')
print()

class Pilot:
    rank_table = {
        'LT': 0,
        'CAPTAIN': 1
    }

    def __lt__(self, pilot2):

        if Pilot.rank_table[self.rank] < Pilot.rank_table[pilot2.rank]:
            return True
        else:
            return False

    def __init__(self, name, rank):
        self.name = name
        self.rank = rank


a = Pilot('Krowso', 'CAPTAIN')
b = Pilot('Titus', 'LT')
if a < b:
    print(f'{a.name} has a lower rank than {b.name}')
else:
    print(f'{b.name} has a lower rank than {a.name}')

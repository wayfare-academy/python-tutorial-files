#!/usr/bin/env python3

x = input("Enter an integer: ")
if x.isnumeric():
    x = int(x)
    print(x+3)
else:
    print(f'{x} is not an integer')
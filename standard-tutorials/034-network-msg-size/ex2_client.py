#!/usr/bin/env python3

import socket

SERVER = '127.0.0.1'
PORT = 10000
MSGS = [
    'This is the longest message that can be sent.',
    'Test 123 Test',
    'Small msg',
    'AAAAAAAAAAAAAAAAAAAAAAAAA'
]

def main():
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as client:
        client.connect((SERVER, PORT))
        for msg in MSGS:
            print(f'Sending: \'{msg}\'')
            msg = msg + '###'
            client.send(msg.encode())
    
main()
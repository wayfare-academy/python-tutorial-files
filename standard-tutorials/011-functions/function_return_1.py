#!/usr/bin/env python3

def say_hello(id=12345, ship='hawk', name='User'):
    print(f'Name: {name}')
    if type(id) is int:
        print(f'ID: {id}')
    else:
        return False
    print(f'Ship: {ship}')
    return True

if say_hello(ship='Cargo', id=23456, name'Krowso'):
    print('Printed out pilot info')
print()
if say_hello(ship='Cargo', id='One-Two-Three', name'Krowso'):
    print('Printed out pilot info')


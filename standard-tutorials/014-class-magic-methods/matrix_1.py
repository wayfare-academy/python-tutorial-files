
class Matrix:

    def __str__(self):
        return f'| {self.a} {self.b} |\n| {self.c} {self.d} |\n'
    
    def __repr__(self):
        return f'Matrix({self.a}, {self.b}, {self.c}, {self.d})'

    def __init__(self, a, b, c, d):
        #  | a b |
        #  | c d |
        self.a = a
        self.b = b
        self.c = c
        self.d = d

a = Matrix(1, 2, 3, 4)
print(a)
print(repr(a))

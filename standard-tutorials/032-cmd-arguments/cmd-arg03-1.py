#!/usr/bin/env python3

'''
Example 3 demonstrating argument groups.
'''

import argparse

parser = argparse.ArgumentParser(description=__doc__)

pgroup = parser.add_argument_group('Pilot Info')
pgroup.add_argument('--pilot-name', help='Specify pilot name')
pgroup.add_argument('--pilot-id', help='Specify pilot ID')

sgroup = parser.add_argument_group('Ship Info')
sgroup.add_argument('--ship-name', help='Specify ship name')
sgroup.add_argument('--ship-type', help='Specify ship type')

args = parser.parse_args()

print()
print(args)
print()

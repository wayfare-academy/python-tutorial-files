#!/usr/bin/env python3

'''
Example 2 demonstrating how to add arguments.
'''

import argparse

parser = argparse.ArgumentParser(description=__doc__)

# list of values for arguments
parser.add_argument('--names1', metavar='n', type=str, nargs=3, help='List of 3 names')
parser.add_argument('--names2', metavar='n', type=str, nargs='?', help='Can optionally pass in name')
parser.add_argument('--names3', metavar='n', type=str, nargs='*', help='ANY number of names, or none at all')
parser.add_argument('--names4', metavar='n', type=str, nargs='+', help='Any number of names, minimum of 1')

args = parser.parse_args()
print(args)


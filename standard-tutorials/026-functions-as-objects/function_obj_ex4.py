#!/usr/bin/env python3

def func1():
    print('hello')

def func2():
    print('bye')

func1()

func1 = func2

func1()
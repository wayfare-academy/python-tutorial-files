#!/usr/bin/env python3

import multiprocessing
import time
from colorama import Fore

dock1_queue = multiprocessing.Queue()
dock2_queue = multiprocessing.Queue()

class DockHandler(multiprocessing.Process):

    def __init__(self, name, color, queue):
        super().__init__()
        self.name = name
        self.color = color
        self.queue = queue

    def run(self):

        while True:
            next = self.queue.get()
            if next == 'EOF':
                break
            print(f'{self.color}{self.name}: {next}{Fore.RESET}')
            time.sleep(.1)

def main():
    dh1 = DockHandler('DOCK 1', Fore.LIGHTBLUE_EX, dock1_queue)
    dh2 = DockHandler('DOCK 2', Fore.YELLOW, dock2_queue)

    dh1.start()
    dh2.start()

    for x in range(50):
        if x % 2 == 0:
            dock1_queue.put(f'Ship {x} docked')
        else:
            dock2_queue.put(f'Ship {x} docked')
        time.sleep(.1)

    for x in range(50):
        if x % 2 == 0:
            dock1_queue.put(f'Ship {x} departed')
        else:
            dock2_queue.put(f'Ship {x} departed')
        time.sleep(.1)

    dock1_queue.put('EOF')
    dock2_queue.put('EOF')

    dh1.join()
    dh2.join()

main()
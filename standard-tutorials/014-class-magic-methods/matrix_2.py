
class Matrix:

    def __add__(self, m2):
        # x = a + b
        temp = Matrix(0,0,0,0)
        temp.a = self.a + m2.a
        temp.b = self.b + m2.b
        temp.c = self.c + m2.c
        temp.d = self.d + m2.d
        return temp

    def __str__(self):
        return f'| {self.a} {self.b} |\n| {self.c} {self.d} |\n'
    
    def __repr__(self):
        return f'Matrix({self.a}, {self.b}, {self.c}, {self.d})'

    def __init__(self, a, b, c, d):
        #  | a b |
        #  | c d |
        self.a = a
        self.b = b
        self.c = c
        self.d = d

a = Matrix(1, 2, 3, 4)
b = Matrix(5, 5, 5, 5)
c = a + b
print(c)

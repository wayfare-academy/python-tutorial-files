#! /usr/bin/env python3

import socket

MSG_LEN = 30
HOST = '127.0.0.1'
PORT = 10000

def main():
    buffer = ''
    recv = []
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as server:
        server.bind((HOST, PORT))
        server.listen()
        conn, addr = server.accept()
        print(f'Connected to {addr}')

        while True:
            data = conn.recv(MSG_LEN)
            data = data.decode()

            if len(data) == 0:
                conn.close()
                break

            print(data)
            if '###' in data:
                while '###' in data:
                    buf, sep, data = data.partition('###')
                    buffer += buf
                    recv.append(buffer)
                    buffer = ''
                buffer += data
            else:
                buffer += data
    
    print('\nMessages received:')
    for r in recv:
        print(r)

main()

class Item:

    def __init__(self, name, desc, value, rarity, count):
        self.name = name
        self.desc = desc
        self.value = value
        self.rarity = rarity
        self.count = count
        print(f'Created Item object {self.name}')

class Equipment(Item):

    def __init__(self, name, desc, value, rarity, count, slot):
        Item.__init__(self, name, desc, value, rarity, count)
        self.slot = slot
        print(f'Created Equipment object {self.name}')

    def equip(self):
        print(f'Equipped {self.name} to {self.slot}')

class Armor(Equipment):

    def __init__(self, name, desc, value, rarity, count, slot, defense):
        super().__init__(name, desc, value, rarity, count, slot)
        self.defense = defense
        print(f'Created Armor object {self.name}')

    def get_defense(self):
        return self.defense

class Weapon(Equipment):

    def __init__(self, name, desc, value, rarity, count, slot, damage):
        super(Weapon, self).__init__(name, desc, value, rarity, count, slot)
        self.damage = damage
        print(f'Created Weapon object {self.name}')

    def attack(self):
        return self.damage

    def equip(self):
        print('Checking weapon skill')
        super().equip()

armor1 = Armor('Leather Helm', 'A Helmet made of leather', 1.0, 'Common', 1, 'Head', 5.0)
print()
armor2 = Armor('Leather Chestpiece', 'A chest piece made of leather', 2.0, 'Common', 1, 'Chest', 12.0)
print()
weapon = Weapon('Iron Sword', 'A sword made of iron', 1.0, 'Common', 1, 'Weapon', 7.0)
print()
item = Item('Paper', 'A piece of paper', 0.1, 'Junk', 999)
print()

armor1.equip()
armor2.equip()
weapon.equip()
print()

print(f'Armor1 Defense: {armor1.get_defense()}')
print(f'Armor2 Defense: {armor2.get_defense()}')
print(f'Attack: {weapon.attack()}')
print()

for i in [item, armor1, armor2, weapon]:
    print(f'Item Name: {i.name}')
    print(f'Item Description: {i.desc}')
    print(f'Item Value: {i.value}')
    print(f'Item Rarity: {i.rarity}')
    print(f'Item Count: {i.count}')
    if isinstance(i, Equipment):
        print(f'Item Slot: {i.slot}')
    if isinstance(i, Armor):
        print(f'Item Defense: {i.defense}')
    if isinstance(i, Weapon):
        print(f'Item Damage: {i.damage}')
    
    print()

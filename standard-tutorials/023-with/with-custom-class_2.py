#!/usr/bin/env python3

from contextlib import contextmanager

class CustomWriter:

    def __init__(self, fname):
        self.fname = fname

    @contextmanager
    def open_file(self):
        try:
            file = open(self.fname, 'w')
            yield file
        finally:
            file.close()

cw = CustomWriter('out2.txt')
with cw.open_file() as fout:
    print('Writing lines to file')
    fout.write('CmW Line 1\n')
    fout.write('CmW Line 2\n')
    fout.write('CmW Line 3\n')

#!/usr/bin/env python3

while (x := input('User input: ')) != 'q':
    if x == '':
        print('User input cannot be blank')
    else:
        print(x)
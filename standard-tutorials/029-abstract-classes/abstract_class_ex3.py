#!/usr/bin/env python3

# THE 'API' CODE
from abc import ABC, abstractmethod

class PluginManager:

    def __init__(self):
        self.plugins = []

    def load(self, plugin):
        self.plugins.append(plugin)

    def run(self):
        for p in self.plugins:
            if not p.verify():
                print(f'{str(p)} could not be verified')
            else:
                p.run()
            print('----------------------------')

class Plugin(ABC):

    def __init__(self, name):
        self.name = name

    @abstractmethod
    def verify(self):
        """ Verify the the plugin can run """

    @abstractmethod
    def run(self):
        """ Run the plugin """

# OUR CODE THAT INTERACTS WITH THE 'API' CODE
import random
class TestPlugin(Plugin):

    def verify(self):
        print(f'Verified TestPlugin {self.name} can run')
        return True
    
    def run(self):
        print(f'Running TestPlugin {self.name}')

class PilotIDPlugin(Plugin):

    def verify(self):
        if self.name != '':
            print(f'Verified that supplied name is valid')
            return True
        return False

    def run(self):
        p_id = random.randint(100000, 999999)
        print(f'ID {p_id} generated for pilot {self.name}')

print('----------------------------')
plm = PluginManager()
plm.load(TestPlugin('Default'))
plm.load(PilotIDPlugin('Krowso'))
plm.load(PilotIDPlugin(''))
plm.load(PilotIDPlugin('Lilith'))
plm.run()
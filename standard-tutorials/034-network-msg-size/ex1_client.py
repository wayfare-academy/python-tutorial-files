#!/usr/bin/env python3

import socket

SERVER = '127.0.0.1'
PORT = 10000
MSG_LEN = 45
MSGS = [
    'This is the longest message that can be sent.',
    'Test 123 Test',
    'AAAAAAAAAAAAAAAAAAAAAAAAA'
]

def main():
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as client:
        client.connect((SERVER, PORT))
        for msg in MSGS:
            print(f'Sending: \'{msg}\'')
            if len(msg) < MSG_LEN:
                print('Padding message')
                pad = '#' * (MSG_LEN - len(msg))
                msg = msg + pad
            client.send(msg.encode())
    
main()
#! /usr/bin/env python3

class Pilot:

    def __init__(self, fname, lname):
        self._first_name = fname
        self._last_name = lname

    @property
    def first_name(self):
        return self._first_name
    
    @first_name.setter
    def first_name(self, value):
        self._first_name = value
    
    @property
    def last_name(self):
        return self._last_name
    
    @last_name.setter
    def last_name(self, value):
        self._last_name = value

    @property
    def full_name(self):
        return f'{self._first_name} {self._last_name}'

p = Pilot('Victor', 'Rotciv')
print(p.full_name)
p.first_name = 'Krowso'
print(p.full_name)
p.last_name = 'Oswork'
print(p.full_name)
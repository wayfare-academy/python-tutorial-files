 #!/usr/bin/env python3

def say_hello(name):
    print(f'Hello {name}')

say_hello('Krowso')
say_hello('Beth')
say_hello(123456)

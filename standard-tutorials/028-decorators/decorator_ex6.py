#!/usr/bin/env python3

class DecClass:

    DEC_COUNT = 0

    def __init__(self, func):
        self.func = func

    def __call__(self, *args, **kwargs):
        print(f'Decorated {self.func} executing')
        ret = self.func(*args, **kwargs)
        DecClass.DEC_COUNT += 1
        print(f'Functions decorated by DecClass: {DecClass.DEC_COUNT}')
        return ret

@DecClass
def func1(msg):
    print(msg)

@DecClass
def func2(x, y):
    return x*y

func1('Test Message')
print()
print(func2(2, 3))
print()
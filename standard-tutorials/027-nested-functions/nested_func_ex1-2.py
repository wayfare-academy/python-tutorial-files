
def outer():
    def inner(x, y):
        return x + y

    return inner(2, 4)

x = outer()
print(x)
#! /usr/bin/env python3

# properties for dictionaries

class Pilot:

    def __init__(self):
        self._data = {
            'name': 'krowso',
            'id': 123456
        }

    def _get_data(self):
        return self._data
    
    data = property(_get_data, None, None,)


p = Pilot()
p.data['id'] = 444444
p.data['ship'] = 'Frigate'

print(p.data)
print(p._data)
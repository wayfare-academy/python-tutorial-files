#!/usr/bin/env python3

from abc import ABC, abstractmethod

class Ship(ABC):
    
    @abstractmethod
    def connect_to_station(self):
        ...

class SmallShip(Ship):
    ...

class LargeShip(Ship):
    ...

class Fighter(SmallShip):
    def connect_to_station(self):
        print('Getting Landing Clearance')
        print('Lands in docking bay')

class Carrier(LargeShip):
    ...

f = Fighter()
f.connect_to_station()

c = Carrier()
c.connect_to_station()
#!/usr/bin/env python3

# scope: parameter variable access in python

def upper_or_lower(typ):
    def wrapper(msg):
        print(f'From wrapper func: typ is {typ}')
        if typ == 'upper':
            return msg.upper()
        elif typ == 'lower':
            return msg.lower()
    print(f'From upper_or_lower func: typ is {typ}')
    return wrapper

l = upper_or_lower('lower')
print(l('THIS SENTENCE WILL BE CHANGED TO ALL LOWERCASE'))
print()
u = upper_or_lower('upper')
print(u('this sentence will be changed to all uppercase'))
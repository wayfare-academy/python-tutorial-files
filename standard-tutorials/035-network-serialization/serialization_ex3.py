#!/usr/bin/env python3

import socket
import pickle

class Pilot:

    def __init__(self, name, id):
        self.name = name
        self.id = id

    def add_ship(self, ship):
        self.ship = ship

class Ship:
    
    def __init__(self, name, typ):
        self.name = name
        self.type = typ

def main():

    pilot = Pilot('Krowso', 12345)
    pilot.add_ship(Ship('Hawk', 'Frigate'))

    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as client:
        client.connect(('127.0.0.1', 10000))
        client.send(pickle.dumps(pilot))
        client.close()

main()


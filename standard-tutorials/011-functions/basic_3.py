 #!/usr/bin/env python3

def say_hello(name='User'):
    print(f'Hello {name}')

say_hello('Krowso')
say_hello()

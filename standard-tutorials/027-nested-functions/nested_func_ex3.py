
def outer_func():
    def inner_func(x, y):
        return x*y
    return inner_func

f = outer_func()
x = f(5, 6)
print(x)
import logging
from logging.config import dictConfig

log_settings = {
    'version': 1,
    'disable_existing_loggers': True,

    'formatters': {
        'custom': {
            'format': '%(asctime)s | %(levelname)8s | %(message)s'
        }
    },

    'handlers': {
        'console': {
            'class': 'logging.StreamHandler',
            'level': 'DEBUG',
            'formatter': 'custom'
        },
        'file': {
            'class': 'logging.FileHandler',
            'level': 'DEBUG',
            'formatter': 'custom',
            'filename': './log5.out',
            'mode': 'w'
        }
    },
    'loggers': {
        'custom_logger': {
            'level': 'DEBUG',
            'handlers': ['console', 'file'],
        }
    }
}

dictConfig(log_settings)
c_log = logging.getLogger('custom_logger')

print()
c_log.debug('This is a debug message')
c_log.info('This is an info message')
c_log.warning('This is a warning message')
c_log.error('This is an error message')
c_log.critical('This is a critical message')
print()
#!/usr/bin/env python3

def say_hello(id=12345, ship='hawk', name='User'):
    print(f'Name: {name}')
    if type(id) is int:
        print(f'ID: {id}')
    else:
        return False, None
    print(f'Ship: {ship}')
    return True, name

x = say_hello()
print(type(x))
print(x)

status, name = say_hello()
print(type(status))
print(type(name))
print(status)
print(name)

#!/usr/bin/env python3
# text based menu

choice = 0

while choice != 'q':
    print('Station Menu')
    print('------------')
    print('1 )  Ships compatible with docks')
    print('2 )  Customs status')
    print('3 )  Request permission to dock')
    choice = input('Choose an option (q to quit): ')
    print()

    if choice == '1':
        print('Shuttles, Frigates, and Small Cruisers may dock here')
    elif choice == '2':
        print('All ships must submit to customs inspection')
    elif choice == '3':
        print('Cleared to land at Dock 1')
    print()
    print()
#!/usr/bin/env python3

class cls_decorator:

    DEC_COUNT = 0

    def __init__(self, cls):
        self.cls = cls

    def __call__(self, *args, **kwargs):
        print(f'Wrapping class: {self.cls}')
        obj = self.cls(*args, **kwargs)
        cls_decorator.DEC_COUNT += 1
        print(f'Classes Decorated: {cls_decorator.DEC_COUNT}')
        return obj

@cls_decorator
class Pilot:

    def __init__(self, name):
        self.name = name

p = Pilot('Krowso')
print(p.name)

b = Pilot('Ben')
print(b.name)
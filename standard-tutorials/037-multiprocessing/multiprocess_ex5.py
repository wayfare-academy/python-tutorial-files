#!/usr/bin/env python3

import multiprocessing
import time
from colorama import Fore

class DockHandler(multiprocessing.Process):

    def __init__(self, name, color, queue, lock):
        super().__init__()
        self.name = name
        self.color = color
        self.queue = queue
        self.lock = lock
        self.pads = []

    def run(self):
        open = True
        while open:
            with self.lock:
                print(f'{self.color}------- {self.name} updates -------{Fore.RESET}')
                if len(self.pads) > 0:
                    dprt = self.pads.pop()
                    print(f'{self.color}Ship {dprt} has departed{Fore.RESET}')

                while len(self.pads) < 6:
                    next = self.queue.get()
                    if next == 'EOF':
                        open = False
                        break
                    self.pads.append(next)
                    print(f'{self.color}Ship {next} has landed{Fore.RESET}')

                if not open:
                    while len(self.pads) > 0:
                        dprt = self.pads.pop()
                        print(f'{self.color}Ship {dprt} has departed{Fore.RESET}')
                    print(f'{Fore.RED}{self.name} closing down{Fore.RESET}')

                print(f'{self.color}------------------------------{Fore.RESET}')
                print()
            time.sleep(1)

def main():
    qlock = multiprocessing.Lock()
    dock_queue = multiprocessing.Queue()

    dh1 = DockHandler('DOCK 1', Fore.LIGHTBLUE_EX, dock_queue, qlock)
    dh2 = DockHandler('DOCK 2', Fore.YELLOW, dock_queue, qlock)

    dh1.start()
    dh2.start()

    for x in range(20):
        dock_queue.put(x)

    dock_queue.put('EOF')
    dock_queue.put('EOF')

    dh1.join()
    dh2.join()

main()
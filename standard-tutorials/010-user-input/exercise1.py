#!/usr/bin/env python3

pilot_list = [
    ('Tom', 123456, 'Frigate', 'Hawk'),
    ('Beth', 234567, 'Cargo', 'Crate'),
    ('Aron', 345678, 'Cruiser', 'Polaris')
]

while True:
    print('1 ) Add new pilot')
    print('2 ) Remove existing pilot')
    print('3 ) Print pilot info')
    choice = input('Make a menu selection (q to quit): ')

    if choice == 'q':
        break
    elif choice == '1':
        name = input('Pilot Name: ')
        p_id = input('Pilot ID: ')
        ship_type = input('Ship Type: ')
        ship_name = input('Ship Name: ')
        pilot_list.append((name, int(p_id), ship_type, ship_name))
    elif choice == '2':
        p_id = input('Enter pilot ID: ')
        p_id = int(p_id)
        for x in range(len(pilot_list)):
            if pilot_list[x][1] == p_id:
                del pilot_list[x]
                break
    elif choice == '3':
        p_id = input('Enter pilot ID: ')
        p_id = int(p_id)
        for x in range(len(pilot_list)):
            if pilot_list[x][1] == p_id:
                print(pilot_list[x])
                break
    else:
        print('Invalid Choice')
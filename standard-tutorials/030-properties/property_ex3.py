#! /usr/bin/env python3

# Properties without setter or deleter

class Pilot:

    def __init__(self, name, id):
        self._name = name
        self._id = id

    # getters
    def _get_name(self):
        return self._name
    
    def _get_id(self):
        return self._id
    
    name = property(_get_name, None, None, 'The name of the pilot')
    p_id = property(_get_id, None, None, 'The ID of the pilot')


p1 = Pilot('Krowso', 123456)
p2 = Pilot('Anna', 234567)

print('\nInitial Values')
print('--------------')
print(f'{p1.name} - {p1.p_id}')
print(f'{p2.name} - {p2.p_id}')

print('\nChange Values')
print('-------------')
p1.name = 'Victor'
print(f'{p1.name} - {p1.p_id}')
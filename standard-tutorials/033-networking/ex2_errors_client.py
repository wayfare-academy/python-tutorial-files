#!/usr/bin/env python3
import argparse
import socket

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('exercise')
    args = parser.parse_args()

    if args.exercise == 'timeout':
        try:
            client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            client.settimeout(3)
            raise TimeoutError()
        #except TimeoutError as e:
        except socket.error as e:
            print('TimeoutError via socket.error')
    elif args.exercise == 'conn':
        try:
            client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            client.connect(('127.0.0.1', 10000))
        #except ConnectionError as e:
        except socket.error as e:
            print('ConnectionError caught by socket.error')
            print(e)
    elif args.exercise == 'broken':
        try:
            client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            client.connect(('127.0.0.1', 10000))
            while True:
                u_in = input('(close to disconnect)> ')
                client.send(u_in.encode())
                if u_in == 'close':
                    client.close()
                    break
        #except BrokenPipeError as e:
        except socket.error as e:
            print('BrokenPipeError caught by socket.error')
            print(e)

main()
import logging
from logging.config import fileConfig


fileConfig('./logging_conf_ex.conf')
c_log = logging.getLogger('c_logger')

print()
c_log.debug('This is a debug message')
c_log.info('This is an info message')
c_log.warning('This is a warning message')
c_log.error('This is an error message')
c_log.critical('This is a critical message')
print()
<h1 align='center'>Python Code Tutorials</h1>

This is a repository containing all of the Jupyter Notebook and Python files
used in the Python tutorial videos on my Youtube Channel:
[Wayfare Coding Academy](https://www.youtube.com/@wayfare_coding_academy)

In order to use the Jupyter Notebook files, you will either need the Jupyter
Notebook software or a code editor that supports these file (i.e. VScode with
the Jupyter Plugin)

NOTE: Where it was deemed necessary, a lot of renaming and numbering has been
done in order to break up code between different sections that happen in the
video. But I have made sure to link all videos with the correct set of code
files below.

## Standard Tutorials
1. [Getting Started](https://youtu.be/wrIhD8_tRII) ([code](standard-tutorials/000-intro.py))
2. [Data Types](https://youtu.be/vU1tNpaMMxo) ([code](standard-tutorials/001-data-types.ipynb))
3. [Data Structures](https://youtu.be/6bdouwlvZyo) ([code](standard-tutorials/002-data-structures.ipynb))
4. [Arithmetic Operators](https://youtu.be/fp91pIFQW1A) ([code](standard-tutorials/003-arithmetic_operators.ipynb))
5. [Bitwise Operators](https://youtu.be/eK7A35rezmE) ([code](standard-tutorials/004-bitwise_operators.ipynb))
6. [Assignment Operators](https://youtu.be/pBcoIxdcvTo) ([code](standard-tutorials/005-assignment_operators.ipynb))
7. [Casting](https://youtu.be/ycvCfdQ2iIM) ([code](standard-tutorials/006-casting.ipynb))
8. [Conditionals](https://youtu.be/jOCFXHUM_nw) ([code](standard-tutorials/007-conditionals.ipynb))
9. [For Loop](https://youtu.be/-gVROS5Quns) ([code](standard-tutorials/008-for-loops.ipynb))
10. [While Loops](https://youtu.be/3Rr4lRJoJFE) ([code](standard-tutorials/009-while-loops))
11. [User Input](https://youtu.be/0td6id0EwI8) ([code](standard-tutorials/010-user-input))
12. [Functions](https://youtu.be/tiyh0lvJhCQ) ([code](standard-tutorials/011-functions))
13. [Classes Part 1: Basics](https://youtu.be/yTqEw0LpVEo) ([code](standard-tutorials/012-class-basics))
14. [Classes Part 2: Class Variables](https://youtu.be/r1r66Wn4xHM) ([code](standard-tutorials/013-class-variables.py))
15. [Classes Part 3: Magic Methods](https://youtu.be/2u7dP_mmaRM) ([code](standard-tutorials/014-class-magic-methods))
16. [Classes Part 4: Inheritance](https://youtu.be/aqeO-xW_qeo) ([code](standard-tutorials/015-inheritance.py))
17. [Importing](https://youtu.be/vil7I0yyzTE) ([code](standard-tutorials/016-importing))
18. [Comments](https://youtu.be/5UnY_ObLwdk) ([code](standard-tutorials/017-comments))
19. [File I/O Part 1: OS Path Module](https://youtu.be/WOD5pT8RNdc) ([code](standard-tutorials/018-os-demo.ipynb))
20. [File I/O Part 2: Pathlib Module](https://youtu.be/X-zBDcOj10w) ([code](standard-tutorials/019-path-demo-ipynb))
21. [File I/O Part 3: File I/O](https://youtu.be/HXPRU4wlJwo) ([code](standard-tutorials/020-file-io))
22. [Errors and Exceptions](https://youtu.be/CJFIYkM2f58) ([code](standard-tutorials/021-errors))
23. [Finally Statement](https://youtu.be/4kRiZrz3zFE) ([code](standard-tutorials/022-finally))
24. [With Statement](https://youtu.be/uhmdwTsvrAc) ([code](standard-tutorials/023-with/))
25. [Generators](https://youtu.be/OnzWBhoCYN0) ([code](standard-tutorials/024-generators/))
26. [Walrus Operator](https://youtu.be/cCPIEOcQjAY) ([code](standard-tutorials/025-walrus/))
27. [Functions as Objects](https://youtu.be/pnDxwTMEXok) ([code](standard-tutorials/026-functions-as-objects/))
28. [Nested Functions](https://youtu.be/_sdDXww9occ) ([code](standard-tutorials/027-nested-functions/))
29. [Decorators](https://youtu.be/YZY-nzXEzI0) ([code](standard-tutorials/028-decorators/))
30. [Abstract Classes](https://youtu.be/5Efyjk5VPws) ([code](standard-tutorials/029-abstract-classes/))
31. [Properties](https://youtu.be/tVLVy5y1Cfs) ([code](standard-tutorials/030-properties/))
32. [Logging](https://youtu.be/wyyKWil4DTk) ([code](standard-tutorials/031-logging/))
33. [Command Line Arguments](https://youtu.be/9Bl3EP1-JHQ) ([code](standard-tutorials/032-cmd-arguments/))
34. [Sockets Basics](https://youtu.be/Yqh8Rhea6Ao) ([code](standard-tutorials/033-networking/))
35. [Sockets Message Sizes](https://youtu.be/ytK2gZBvJR0) ([code](standard-tutorials/034-network-msg-size/))
36. [Sockets Serialization](https://youtu.be/jgfSsifmoz0) ([code](standard-tutorials/035-network-serialization/))
37. [Multi-threading](https://youtu.be/ou14jf1fdug) ([code](standard-tutorials/036-multithreads/))
38. [Multi-processing](https://youtu.be/Tg2fY_38EtY) ([code](standard-tutorials/037-multiprocessing/))
39. [Advanced String Stuff](https://youtu.be/erWv3y2_aL0) ([code](standard-tutorials/038-advanced-strings/))
40. [Advanced Lists Stuff](https://youtu.be/27kyGFyV0cg) ([code](standard-tutorials/039-advanced-lists/))
41. [Advanced Tuple Stuff] (https://youtu.be/XCFOpFlhnH8) ([code](standard-tutorials/040-advanced-tuples/))
#!/usr/bin/env python3

from abc import ABC, abstractmethod

class Parent(ABC):

    def __init__(self, arg1):
        self.arg1 = arg1

    @abstractmethod
    def func1(self):
        """ Abstract function """


class Child(Parent):

    def __init__(self, arg1, arg2):
        self.arg2 = arg2
        super().__init__(arg1)

    def func1(self):
        print('Abstract method')

c = Child('arg1', 'arg2')
c.func1()